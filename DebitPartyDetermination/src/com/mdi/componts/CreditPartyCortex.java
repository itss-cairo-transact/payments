/**
 * 
 * This class implement two core functions from super class PaymentLifecycle to validate 
 * SWIFT transactions which's contain Card Number and send this information to external
 * third-party Cortex API in order to define card infrormation and append current Trx
 * with the date collected from Cortex.
 * 
 * @author Kareem Mortada <KMortada@itssglobal.com>
 * @author Mouchera Topouzzada <MTopouzzada@itssglobal.com>
 */
package com.mdi.componts;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.temenos.t24.api.complex.pp.paymentlifecyclehook.Account;
import com.temenos.t24.api.complex.pp.paymentlifecyclehook.CreditTransaction;
import com.temenos.t24.api.complex.pp.paymentlifecyclehook.Response;
import com.temenos.t24.api.complex.pp.paymentlifecyclehook.TransactionContext;
import com.temenos.t24.api.hook.payments.PaymentLifecycle;
import com.temenos.t24.api.records.porsupplementaryinfo.PartyTypeClass;
import com.temenos.t24.api.records.porsupplementaryinfo.PorSupplementaryInfoRecord;
import com.temenos.t24.api.system.DataAccess;
import com.temenos.t24.api.tables.ppcortexproductvalues.PpCortexProductValuesRecord;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class CreditPartyCortex extends PaymentLifecycle {
  // Inisialize variables
  private DataAccess dataAccess = new DataAccess(this);
  private HttpHelper httpHelper = new HttpHelper();
 
  @Override
  public Account getCreditAccount(TransactionContext transactionContext, CreditTransaction creditTransaction) {
    // Inisialize variables
    Account account = new Account();
    // Get Transaction data from POR.SUPPLEMENTARY.INFO
    PorSupplementaryInfoRecord porSupplementaryInfo = new PorSupplementaryInfoRecord(
      dataAccess.getRecord("", "POR.SUPPLEMENTARY.INFO", "", transactionContext.getTransactionId())
    );
    
    // Get party type from porSupplementaryInfo
    List<PartyTypeClass> partyType = porSupplementaryInfo.getPartyType();

    // Credit account number as it sent in the transaction
    String creditAccountNum = partyType.get(0).getPartyAccountLine().getValue();

    // Remove any special characters
    creditAccountNum = creditAccountNum.replaceAll("[^0-9]", "");
    
    // Check if the credit account length is 16 digit
    if(creditAccountNum.length() == 16) {
      try {
        // Coretex API end point for card inquiry
        String url = "http://172.19.1.133:7001/cortex-core-ws-rest/inquiry/CustomerAccountCardInquiry";

        // Build JSON request massage
        httpHelper.setRequestData(this.inqueryRequest(creditAccountNum)); //1000000000000077

        // Server response json object
        JSONObject responseObj = httpHelper.httpPOSTRequest(url);

        // Get response code to validate if the response is successfully determiend the card
        String responseCode = (String) httpHelper.getByObjectTree(responseObj, "ResponseHeader").get("ResponseCode");
        
        // Check if the HTTP response status is 200 and the server is successfully determiend the card
        if(httpHelper.getStatusCode() == 200 && responseCode.equals("000")){
          // Get Product code to define card type
          String productCode = (String) httpHelper.getByObjectTree(
            responseObj, "CustomerAccountCardInquiryResponseDetails","CardDetailsList[CardProduct]").get("ProductCode");

          // Check if the product code have an record in CORTEXPRODUCTVALUES
          List<String> productRecordId = dataAccess.selectRecords("BNK", "PP.CORTEXPRODUCTVALUES", "", "WITH @ID EQ \"" + productCode + "\"");

          // Check if there's a record for this product
          if(productRecordId.size() > 0){
            // Get Record data for current product
            PpCortexProductValuesRecord CortexProductValuesRecord = new PpCortexProductValuesRecord(
              dataAccess.getRecord("PP.CORTEXPRODUCTVALUES", productRecordId.get(0).toString()));

            // Set data into account object 
            account.setAccountId(CortexProductValuesRecord.getAccountNumber().getValue()); 
            account.setCompanyId(transactionContext.getCompanyId()); 
            account.setCurrencyId(CortexProductValuesRecord.getCurrency().getValue()); 
          }
          else{
            // If you come here then this's a Debit Card            
            // Get account number from response
            String accountNumner = (String) httpHelper.getByObjectTree(
                  responseObj, "CustomerAccountCardInquiryResponseDetails","CardDetailsList[LinkedAccountsList[AccountIdentifier]]").get("AccountNumber");
            
            // Get currency code from response
            String accountCurrency = (String) httpHelper.getByObjectTree(
                  responseObj, "CustomerAccountCardInquiryResponseDetails","CardDetailsList[LinkedAccountsList[AccountIdentifier]]","Currency").get("AlphaCode");

            // Set data into account object 
            account.setAccountId(accountNumner);
            account.setCompanyId(transactionContext.getCompanyId()); 
            account.setCurrencyId(accountCurrency);
          }
        }
        else {
          // Reset account object as it recived from the transaction
          account = creditTransaction.getAccount();
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
      // Return updated account
      return account;
    }
    else{
      return super.getCreditAccount(transactionContext, creditTransaction);
    }
  }

  
  // Validate Card Number 
  @Override
  public Response validateCreditParty(TransactionContext transactionContext, CreditTransaction creditTransaction) {
    // Get Transaction data from POR.SUPPLEMENTARY.INFO
    PorSupplementaryInfoRecord porSupplementaryInfo = new PorSupplementaryInfoRecord(
      dataAccess.getRecord("", "POR.SUPPLEMENTARY.INFO", "", transactionContext.getTransactionId())
    );

    // Get party type from porSupplementaryInfo
    List<PartyTypeClass> partyType = porSupplementaryInfo.getPartyType();

    // Credit account number as it sent in the transaction
    String creditAccountNum = partyType.get(0).getPartyAccountLine().getValue();

    // Remove any special characters
    creditAccountNum = creditAccountNum.replaceAll("[^0-9]", "");
    
    // Check if the credit account length is 16 digit
    if(creditAccountNum.length() == 16) {
      try {
        // Coretex API end point for card inquiry
        String url = "http://172.19.1.133:7001/cortex-core-ws-rest/inquiry/CustomerAccountCardInquiry";

        // Build JSON request massage
        httpHelper.setRequestData(this.inqueryRequest(creditAccountNum)); //1000000000000077

        // Server response json object
        JSONObject responseObj = httpHelper.httpPOSTRequest(url);

        // Get response header to validate if the response is successfully determiend the card
        JSONObject responseHeader = httpHelper.getByObjectTree(responseObj, "ResponseHeader");
        
        // Check if the HTTP response status is 200 and the server is successfully determiend the card
        if(httpHelper.getStatusCode() == 200 && responseHeader.get("ResponseCode").equals("000")){
          // Get Product code to define card type
          JSONObject cardProduct = httpHelper.getByObjectTree(
            responseObj, "CustomerAccountCardInquiryResponseDetails","CardDetailsList[CardProduct]");

          String description = (String) cardProduct.get("Description");

          // Check if card is Prepaid  
          if(description.toLowerCase().contains("prepaid")){
            // Coretex API end point for card inquiry
            String prepaidUrl = "http://172.19.1.133:7001/cortex-core-ws-rest/prepaidacc/AccountLoad";

            // Setting load prepaid account request to Cortext with amount and currency code
            httpHelper.setRequestData(
              this.loadPREPAIDRequest(creditAccountNum, 
              Double.parseDouble(creditTransaction.getAmount().get()), 
              creditTransaction.getTransaction().getTransactionCurrencyId())
            ); 

            // Cortex response json object
            JSONObject prepaidResponseObj = httpHelper.httpPOSTRequest(prepaidUrl);

            // Get response code to validate if the response is successfully determiend the card
            JSONObject prepaidResponseHeader = httpHelper.getByObjectTree(prepaidResponseObj, "ResponseHeader");
            String prepaidResponseType = (String) prepaidResponseHeader.get("ResponseType");

            // Check if Cortex responded with an bussines error
            if (prepaidResponseType.toLowerCase().contains("error")){
              // Get error code and description from Cortext response
              String responsError = (String) prepaidResponseHeader.get("ResponseCode") + "-" + prepaidResponseHeader.get("ResponseDescription");
              // Return with corresponding error from PP.ERRORCODE table 
              return getErrorResponse("CPD10099",responsError);
            }
            // Code Exit HERE
          }
        }
        else {
          // Get error code and description from Cortext response
          String responsError = (String) responseHeader.get("ResponseCode") + "-" + responseHeader.get("ResponseDescription");
          // Return with corresponding error from PP.ERRORCODE table 
          return getErrorResponse("CPD00006",responsError); // Invalid response from Hook API
        }
        // Code Exit HERE
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    // Return super class if TRX is not contains card number
    return super.validateCreditParty(transactionContext, creditTransaction);
  }

  // Create Cortex inquery request object 
  public JSONObject inqueryRequest(String CardNumber) throws ParseException{
    JsonBuilder JB = new JsonBuilder();
    JB.rootNode(
        JB.addJSONObj("RequestHeader",
            JB.objNodes(
                JB.node("Version", "1.0.0"),
                JB.node("MsgUid", "322cfaee-4dd7-11eb-ae93-0242ac130002"),
                JB.node("Source", "CHANNEL NAME"),
                JB.node("ServiceId", "CardsDetails"),
                JB.node("ReqDateTime", ""),
                JB.node("Locale", "en")
            )
        ),
        JB.addJSONObj("CustomerAccountCardInquiryRequestDetails", 
            JB.addJSONObj("CardIdentifier", 
                JB.objNodes(
                    JB.node("Vpan", CardNumber)
                )
            ),
            JB.addJSONObj("AdditionalData", 
                JB.objNodes(
                    JB.node("ReturnAccountDetails", true),
                    JB.node("ReturnCardProductDetails", true)
                )
            )
        )
    );
    return JB.build();
  }

  // Set response error from PP.ERRORCODE table
  private Response getErrorResponse(String id, String setCustomError){
    Response response = new Response();
    response.setCode(id);
    response.setText(setCustomError);
    // response.setType(id);
    return response;
  }

  // Create Cortex PREPAID request object 
  public JSONObject loadPREPAIDRequest(String CardNumber,Double amount, String currencyAlphaCode) throws ParseException{
    LocalDateTime myDateObj = LocalDateTime.now();  
    DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("ddMMyyyyHHmmss");  
    String randomExtCode = myDateObj.format(myFormatObj); 
    
    JsonBuilder JB = new JsonBuilder();
    JB.rootNode(
        JB.addJSONObj("RequestHeader",
          JB.objNodes(
            JB.node("Version", "1.0.0"),
            JB.node("MsgUid", "322cfaee-4dd7-11eb-ae93-0242ac130002"),
            JB.node("Source", "CHANNEL NAME"),
            JB.node("ServiceId", "ACCLOAD"),
            JB.node("ReqDateTime", ""),
            JB.node("Locale", "en")
          )
        ),
        JB.addJSONObj("AccountLoadRequestDetails", 
            JB.objNodes(
                JB.node("Action", "1")
            ),
            JB.addJSONObj("BaseAccountIdentifier", 
              JB.addJSONObj("CardIdentifier", 
                  JB.objNodes(
                      JB.node("Vpan", CardNumber)
                  )
              ),
              JB.objNodes(
                  JB.node("AccountDerivationMethod", 1)
              )
            ),
            JB.objNodes(
                JB.node("LoadType", 0)
        ),
        JB.objNodes(
                JB.node("LoadSource", 0)
        ),
        JB.addJSONObj("LoadAmount", 
          JB.objNodes(
            JB.node("Amount", amount)
          ),
          JB.addJSONObj("Currency", 
              JB.objNodes(
                  JB.node("AlphaCode", currencyAlphaCode)
              )
          )
        ),
        JB.objNodes(
          JB.node("ExtCode", randomExtCode)
        ),
        JB.objNodes(
          JB.node("Description", "LOAD PREPAID ACCOUNT")
        ) 
      )        
    );
    return JB.build();
  }
}
