package com.mdi.componts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

class HttpHelper {
    protected final String USER_AGENT = "Mozilla/5.0";
    protected final String POST_PARAMS = "userName=Ramesh&password=Pass@123";
    // private List<Map<String,String>> _headerData = new ArrayList<Map<String,String>>();
    private HttpURLConnection httpURLConnection;
    private int _statusCode;
    private String _paramData;
    private JSONObject _getByObjectTree;
    
    protected JSONObject httpPOSTRequest(String url) throws IOException, ParseException {
        URL obj = new URL(url);
        this.httpURLConnection = (HttpURLConnection) obj.openConnection();
        this.httpURLConnection.setRequestMethod("POST");
        this.httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
        this.httpURLConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        this.httpURLConnection.setRequestProperty("Accept", "application/json");
        // Set Header data 

        // For POST only - START
        this.httpURLConnection.setDoOutput(true);
        OutputStream os = httpURLConnection.getOutputStream();
        os.write(this.getRequestData().getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = httpURLConnection.getResponseCode();
        this.setStatusCode(responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            } in.close();
            // print result            
            JSONObject mainObj = (JSONObject) new JSONParser().parse(response.toString());
            return mainObj;
            
        } else {
            return null;
        }
    }
    
    protected JSONObject httpGETRequest(String url) throws IOException, ParseException {
        URL obj = new URL(url);
        HttpURLConnection httpURLConnection = (HttpURLConnection) obj.openConnection();
        httpURLConnection.setRequestMethod("GET");
        httpURLConnection.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = httpURLConnection.getResponseCode();
        if (responseCode == HttpURLConnection.HTTP_OK) { // success
            BufferedReader in = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in .readLine()) != null) {
                response.append(inputLine);
            } in .close();

            JSONObject mainObj = (JSONObject) new JSONParser().parse(response.toString());
            return mainObj;
        } else {
            return null;
        }
    }

    protected JSONArray getObjectArrayByName(JSONArray jsonArray, String key) throws ParseException{
        JSONArray keyMap = new JSONArray();
        for (Object obj : jsonArray.toArray()){
            JSONObject objMap = (JSONObject) new JSONParser().parse(obj.toString()) ;
            if (objMap.containsKey(key)) {
                keyMap = (JSONArray) objMap.get(key);
                return keyMap;
            }
        }
        return null;
    }

    protected JSONArray getObjectArrayByName(JSONObject jsonObj, String key) throws ParseException{
        JSONArray jsonArray = (JSONArray) jsonObj.get(key);
        return jsonArray;
    }

    protected JSONObject getObjectByName(JSONObject jsonObj ,String name) throws ParseException{
        JSONObject keyMap = new JSONObject();
        if(jsonObj.containsKey(name)) {
            keyMap = (JSONObject)jsonObj.get(name);
            return keyMap;
        }
        return null;
    }

    protected JSONObject getObjectByName(JSONArray jsonArray ,String name) throws ParseException{
        JSONObject keyMap = new JSONObject();
        for (Object obj : jsonArray.toArray()){
            JSONObject objMap = (JSONObject) new JSONParser().parse(obj.toString());
            if (objMap.containsKey(name)) {
                keyMap = (JSONObject)objMap.get(name);
                return keyMap;
            }
        }
        return null;
    }

    protected JSONObject getByObjectTree(JSONObject mainObj,String... objectsTree) throws ParseException{
        JSONObject finalObj = mainObj;
        for (String obj : objectsTree){
            if (obj.contains("[")) {
                obj = obj.replace("]", "");
                String[] nestedArr = obj.split("\\[");
                String arrObjName = nestedArr[nestedArr.length-1];
                JSONArray tempNestedArr = new JSONArray();
                for(int i = 0 ; i < nestedArr.length-1 ; i++){
                    if(i == 0) tempNestedArr = this.getObjectArrayByName(finalObj, nestedArr[i]);
                    else tempNestedArr = this.getObjectArrayByName(tempNestedArr, nestedArr[i]);
                }
                finalObj = this.getObjectByName(tempNestedArr, arrObjName);
            }
            else {
                finalObj = this.getObjectByName(finalObj, obj);
            }
        }
        return finalObj;
    }

    protected void setRequestData(JSONObject jsonObject) {
        this._paramData = jsonObject.toString();
    }

    private String getRequestData(){
        return this._paramData;
    }

    private void setStatusCode(int status){
        this._statusCode = status;
    }

    protected int getStatusCode() {
        return this._statusCode;
    }
}
