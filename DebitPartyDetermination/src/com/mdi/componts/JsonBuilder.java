/*
 * Created Date: Thursday, January 20th 2022, 2:49:42 pm
 * Author: kmortada@itssglobal.com
 * 
 * Copyright (c) 2022 Your Company
 */

package com.mdi.componts;

import java.util.Hashtable;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

// Builder Json Object

public class JsonBuilder
{
    private JSONObject _root = new JSONObject();

    /** 
     * Main JSON Object for example return { "root" : {},{}, ... }
     * @param name Root object name
     * @param objects {"key":value},{"key":value},...
     * @throws ParseException
     */
    public void rootNode(JSONObject... objects) throws ParseException {
        for(JSONObject obj : objects){
            this._root.putAll(obj);
        }
    }

    /** 
     * Create new JSON Oject with multiple objects inside for example 
     * return { ... "customer" : { {"address" : "Egypt"},{"phone" : "123456..."},... }
     * @param name Object name
     * @param objects {"key":value},{"key":value},...
     * @return JSONObject
     * @throws ParseException
     */
    public JSONObject addJSONObj(String name,JSONObject... objects) throws ParseException {
        JSONObject objNode = new JSONObject();
        JSONObject finalObj = new JSONObject();
        for(JSONObject obj : objects){
            objNode.putAll(obj);
        }
        finalObj.put(name,objNode);
        return finalObj;
    }

    /** 
     * Create new JSON Oject Array with multiple objects inside for example 
     * return { ... "customer" : [ {"address" : "Egypt"},{"phone" : "123456..."},... ]
     * @param name Object name
     * @param objects {"key":value},{"key":value},...
     * @return JSONObject
     * @throws ParseException
     */
    public JSONObject addJSONArr(String name,JSONObject... objects) throws ParseException {
        JSONArray arr = new JSONArray();
        JSONObject finalObj = new JSONObject();
        for(JSONObject obj : objects){
            arr.add(obj);
        }
        finalObj.put(name, arr);
        return finalObj;
    }
    
    /** 
     * return an simple JSON object for example {"key" : "value","key" : "value", ...}
     * @param nodes
     * @return JSONObject
     * @throws ParseException
     */
    @SafeVarargs
    public final JSONObject objNodes(Map<String,Object>... nodes) throws ParseException {
        JSONObject obj = new JSONObject();
        for(Map<String, Object> objData : nodes){
            objData.forEach((key,value)->{
                obj.put(key, value);
            });
        }
        return obj;
    }

    
    /** 
     * Create JSON node for example "key" : "value"
     * @param key
     * @param value
     * @return Map<String, String>
     * @throws ParseException
     */
    public Map<String,Object> node(String key,Object value) throws ParseException {
        Map<String,Object> node = new Hashtable<String,Object>();
        node.put(key, value);
        return node;
    }
    
    /** 
     * return a full object of objects you have been customized
     * @return JSONObject
     * @throws ParseException
     */
    public JSONObject build() throws ParseException {
        return this._root;
    }

    
}
