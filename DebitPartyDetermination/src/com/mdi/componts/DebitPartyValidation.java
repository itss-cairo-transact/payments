/**
 * 
 * This class implement one core functions from super class PaymentLifecycle to validate 
 * SWIFT transactions that's received from Egypt banks must have PEG in block 3 {103:PEG}
 * 
 * @author Kareem Mortada <KMortada@itssglobal.com>
 * @author Mouchera Topouzzada <MTopouzzada@itssglobal.com>
 */
package com.mdi.componts;

import java.util.List;

import com.temenos.t24.api.complex.pp.paymentlifecyclehook.DebitTransaction;
import com.temenos.t24.api.complex.pp.paymentlifecyclehook.Response;
import com.temenos.t24.api.complex.pp.paymentlifecyclehook.Transaction;
import com.temenos.t24.api.complex.pp.paymentlifecyclehook.TransactionContext;
import com.temenos.t24.api.hook.payments.PaymentLifecycle;
import com.temenos.t24.api.records.debic.DeBicRecord;
import com.temenos.t24.api.system.DataAccess;

public class DebitPartyValidation extends PaymentLifecycle {
  public Response validateDebitParty(TransactionContext transactionContext, DebitTransaction debitTransaction) {
    // Inisialize variables
    Transaction transcation = new Transaction();
    DataAccess getRecord = new DataAccess();
    
    try {
      // Get transaction object from debitTransaction
      transcation = debitTransaction.getTransaction();
      
      // Get BicCode from debitTransaction
      String trxBicCode = debitTransaction.getSenderBic();
      
      // Get record id which's have the a bicode in his fields as it defined in the transaction
      List<String> pptBicId = getRecord.selectRecords("", "PPT.BICTABLE", "", "WITH BICCode EQ \""+trxBicCode+"\""); //"trxBicCode"

      // Get PPT.BICTABLE record by BICId
      DeBicRecord deBicRecord = new DeBicRecord(getRecord.getRecord("","PPT.BICTABLE", "",pptBicId.get(0)));
      String countryCode = deBicRecord.getCountryCode().getValue();
      String finCopyService = transcation.getOriginatingSource();
      Response response = new Response();
      // Check if Payment Direction is "I" incoming Trx and country is "EG" 
      if (debitTransaction.getOriginatingChannel().equals("SWIFT")) {
        if(countryCode.equals("EG") && !finCopyService.equals("PEG")){
          try {
            // response.setText("MISSINGPEG");
            response.setText("MISSINGPEG");
            response.setType("ERR");
          } catch (Exception e) {
            e.printStackTrace();
          } 
          return response;
        }
      } 
    } catch (Exception e) {
      e.printStackTrace();
    } 
    return super.validateDebitParty(transactionContext, debitTransaction);
  }
}
