package com.temenos.t24.api.tables.ppcortexproductvalues
public class PpCortexProductValuesTable {
    /**
    * Returns an unmodifiable list of ID of the record satisfying the 'where' clause  for 'F.PP.CORTEX.PRODUCT.VALUES' 
    * example : List&lt;String&gt; IDs = myTable.select("ACTION like 'ABC%'")<br>
    * @param whereClause java.lang.CharSequence : 'Where clause' part of a sql statement.<br>
    * @return List<String> : <br>
    */
    public List<String> select(java.lang.CharSequence whereClause){}

    /**
    * Read a PpCortexProductValuesRecord from 'F.PP.CORTEX.PRODUCT.VALUES"<br>
    * @param id java.lang.CharSequence : Record ID to read<br>
    * @return com.temenos.t24.api.tables.ppcortexproductvalues.PpCortexProductValuesRecord : <br>
    */
    public com.temenos.t24.api.tables.ppcortexproductvalues.PpCortexProductValuesRecord read(java.lang.CharSequence id){}

    /**
    * Release (unlock)  the table 'F.PP.CORTEX.PRODUCT.VALUES' for the specified ID<br>
    * @param id java.lang.CharSequence : Record ID to release<br>
    * @return boolean : <br>
    */
    public boolean release(java.lang.CharSequence id){}

    /**
    * Clear the table 'F.PP.CORTEX.PRODUCT.VALUES<br>
    * @return boolean : <br>
    */
    public boolean clear(){}

    /**
    * Lock the table 'F.PP.CORTEX.PRODUCT.VALUES' for the specified ID<br>
    * @param id java.lang.CharSequence : Record ID to lock<br>
    * @return boolean : <br>
    */
    public boolean lock(java.lang.CharSequence id){}

    /**
    * Returns an unmodifiable list of all record IDs of 'F.PP.CORTEX.PRODUCT.VALUES'<br>
    * @return List<String> : <br>
    */
    public List<String> select(){}

    /**
    * Write a PpCortexProductValuesRecord to 'F.PP.CORTEX.PRODUCT.VALUES"<br>
    * @param id java.lang.CharSequence : Record ID to read<br>
    * @param record PpCortexProductValuesRecord : Record to write<br>
    * @return boolean : <br>
    */
    public boolean write(java.lang.CharSequence id, PpCortexProductValuesRecord record){}

    /**
    * Delete a PpCortexProductValuesRecord of 'F.PP.CORTEX.PRODUCT.VALUES"<br>
    * @param id java.lang.CharSequence : Record ID to delete<br>
    * @return boolean : <br>
    */
    public boolean delete(java.lang.CharSequence id){}

}
