package com.temenos.t24.api.tables.ppcortexproductvalues
/**
*
*/
public class PpCortexProductValuesRecord {
    /**
    * Returns the Override at the specified position in this collection. 
    * Throws an IndexOutOfBoundsException if index out of range of values.<br>
    * @param idx int : idx of the element to return<br>
    * @return TField : <br>
    */
    public TField getOverride(int idx){}

    /**
    * Returns the collection of Inputter <br>
    * @return List<String> : <br>
    */
    public List<String> getInputter(){}

    /**
    * Returns the RecordStatus <br>
    * @return String : The RecordStatus<br>
    */
    public String getRecordStatus(){}

    /**
    * Returns the Inputter at the specified position in this collection. 
    * Throws an IndexOutOfBoundsException if index out of range of values.<br>
    * @param idx int : idx of the element to return<br>
    * @return String : <br>
    */
    public String getInputter(int idx){}

    /**
    * Returns the CurrNo <br>
    * @return String : The CurrNo<br>
    */
    public String getCurrNo(){}

    /**
    * Set the specified value for AccountNumber<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param value java.lang.CharSequence : The value to set<br>
    */
    public void setAccountNumber(java.lang.CharSequence value){}

    /**
    * Returns the collection of DateTime <br>
    * @return List<String> : <br>
    */
    public List<String> getDateTime(){}

    /**
    * Set the specified Override to a specific position in collection. <br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param param TField : the Override to append<br>
    * @param idx int : the position in collection<br>
    */
    public void setOverride(TField param, int idx){}

    /**
    * Return the date/time of the creation of this class<br>
    * @return java.lang.String : the date/time of the creation of this class<br>
    */
    public java.lang.String getBuildDate(){}

    /**
    * Returns the DateTime at the specified position in this collection. 
    * Throws an IndexOutOfBoundsException if index out of range of values.<br>
    * @param idx int : idx of the element to return<br>
    * @return String : <br>
    */
    public String getDateTime(int idx){}

    /**
    * Returns the Currency <br>
    * @return TField : The Currency<br>
    */
    public TField getCurrency(){}

    /**
    * Returns the collection of Override <br>
    * @return List<TField> : <br>
    */
    public List<TField> getOverride(){}

    /**
    * Appends the specified Override to the end of the collection.<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param param java.lang.CharSequence : the Override to append<br>
    */
    public void addOverride(java.lang.CharSequence param){}

    /**
    * Returns the AuditorCode <br>
    * @return String : The AuditorCode<br>
    */
    public String getAuditorCode(){}

    /**
    * Set the specified value for Currency<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param value java.lang.CharSequence : The value to set<br>
    */
    public void setCurrency(java.lang.CharSequence value){}

    /**
    * Insert the specified Override to the specified position in the collection. <br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param param TField : the Override to append<br>
    * @param pos int : The position where to insert the value.<br>
    */
    public void insertOverride(TField param, int pos){}

    /**
    * Appends the specified Override to the end of the collection. <br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param param TField : the Override to append<br>
    */
    public void addOverride(TField param){}

    /**
    * Return if this object is modifiable or not<br>
    */
    public void isModifiable(){}

    /**
    * Returns the AccountNumber <br>
    * @return TField : The AccountNumber<br>
    */
    public TField getAccountNumber(){}

    /**
    * Return the compiler version used to create this class<br>
    * @return java.lang.String : the compiler vertion used to create this class<br>
    */
    public java.lang.String getBuildVersion(){}

    /**
    * Returns the DeptCode <br>
    * @return String : The DeptCode<br>
    */
    public String getDeptCode(){}

    /**
    * Set the specified value for Currency<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param value TField : The value to set<br>
    */
    public void setCurrency(TField value){}

    /**
    * Insert the specified Override to the specified position in the collection. <br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param param java.lang.CharSequence : the Override to append<br>
    * @param pos int : The position where to insert the value.<br>
    */
    public void insertOverride(java.lang.CharSequence param, int pos){}

    /**
    * Returns the AuditDateTime <br>
    * @return String : The AuditDateTime<br>
    */
    public String getAuditDateTime(){}

    /**
    * Return a new Generic structure from this record object<br>
    * @return TStructure : <br>
    */
    public TStructure toStructure(){}

    /**
    * Remove the Override at the specified position in this collection.<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param idx int : index of the element to remove<br>
    */
    public void removeOverride(int idx){}

    /**
    * Clear the collection of Override<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    */
    public void clearOverride(){}

    /**
    * Returns the Authoriser <br>
    * @return String : The Authoriser<br>
    */
    public String getAuthoriser(){}

    /**
    * Set the specified value for AccountNumber<br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param value TField : The value to set<br>
    */
    public void setAccountNumber(TField value){}

    /**
    * Set the specified Override to a specific position in collection. <br>Throws an UnsupportedOperationException if the object is not modifiable. <br>
    * @param param java.lang.CharSequence : the Override to append<br>
    * @param idx int : the position in collection<br>
    */
    public void setOverride(java.lang.CharSequence param, int idx){}

    /**
    * Returns the CoCode <br>
    * @return String : The CoCode<br>
    */
    public String getCoCode(){}

}
