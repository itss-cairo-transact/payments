package com.mdi.version;

import com.temenos.api.TField;
import com.temenos.api.TStructure;
import com.temenos.t24.api.complex.eb.templatehook.InputValue;
import com.temenos.t24.api.complex.eb.templatehook.TransactionContext;
import com.temenos.t24.api.hook.system.RecordLifecycle;
import com.temenos.t24.api.records.paymentorder.PaymentOrderRecord;

public class PaymentOrderBenfBicAutoFill extends RecordLifecycle {
  @Override 
  public void defaultFieldValuesOnHotField(String application, String currentRecordId, TStructure currentRecord,
      InputValue currentInputValue, TStructure unauthorisedRecord, TStructure liveRecord,
      TransactionContext transactionContext) {
        
        TField bankToBankInfo  = new TField();
        PaymentOrderRecord paymentOrderRec = new PaymentOrderRecord(currentRecord);
        TField beneficiaryBicCode  = paymentOrderRec.getAcctWithBankBic();
        switch (beneficiaryBicCode.getValue()) {
          case "CBEGEGCXXXX":
            bankToBankInfo.setValue("SR-/REC/ACCTR");
            break;
          case "CBEGEGCXALX":
            bankToBankInfo.setValue("SR-/REC/ALXTR");
            break;
        } 
        paymentOrderRec.setBankToBankInfo(bankToBankInfo,0);
        currentRecord.set(paymentOrderRec.toStructure());
  }
}
