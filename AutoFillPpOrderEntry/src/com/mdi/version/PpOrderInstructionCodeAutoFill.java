package com.mdi.version;

import com.temenos.api.TField;
import com.temenos.api.TStructure;
import com.temenos.t24.api.complex.eb.templatehook.InputValue;
import com.temenos.t24.api.complex.eb.templatehook.TransactionContext;
import com.temenos.t24.api.hook.system.RecordLifecycle;
import com.temenos.t24.api.records.pporderentry.PpOrderEntryRecord;

//PP.ORDER.AUTO.FILL 99.1
public class PpOrderInstructionCodeAutoFill extends RecordLifecycle {
  @Override
  public void defaultFieldValuesOnHotField(String application, String currentRecordId, TStructure currentRecord,
      InputValue currentInputValue, TStructure unauthorisedRecord, TStructure liveRecord,
      TransactionContext transactionContext) {
         
        TField instructionCode = new TField();
        PpOrderEntryRecord orderEnteryRec = new PpOrderEntryRecord(currentRecord);
        TField ReceiverInst = orderEnteryRec.getReceiverinstitutionbic();
        switch (ReceiverInst.getValue()) {
          case "CBEGEGCXXXX":
            instructionCode.setValue("SR-/REC/ACCTR");
            break;
          case "CBEGEGCXALX":
            instructionCode.setValue("SR-/REC/ALXTR");
            break;
        } 
        orderEnteryRec.setInstructioncode(instructionCode, 0);
        currentRecord.set(orderEnteryRec.toStructure());
  }
}
